(add-to-list 'load-path "~/src/habij/lib")

(require 'screencast)
(require 'pkgmgr)
(require 'elisp)
(require 'editor)

(screencast-init)

;; Package management
(pkgmgr-init)

(pkgmgr-package-install
 '(json-mode
   paredit
   parinfer
   dracula-theme
   rainbow-delimiters
   ido-vertical-mode))

(load-theme 'dracula t)
(load-elisp-setup)

(editor-init-ido)
