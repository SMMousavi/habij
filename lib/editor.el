
(defun editor-init-ido ()
  (require 'ido)
  (require 'ido-vertical-mode)

  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1)
  (ido-vertical-mode 1))


(provide 'editor)
