(require 'package)

(defun pkgmgr-package-install (packages)
  "Make sure that the given PACKAGES are installed in the system."
  (mapcar 'package-install packages))

(defun pkgmgr-init ()
  "Initilize the package manager to Habij."
  (setq package-user-dir "~/src/habij/packages")
  (add-to-list 'package-archives
	       '("melpa" . "http://melpa.org/packages/"))
  (package-initialize))


(provide 'pkgmgr)
